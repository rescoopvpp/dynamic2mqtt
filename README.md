# Dynamic2MQTT

Takes dynamic pricing (currently from Octopus Agile) and publishes to MQTT, in a way suitable for Home Assistant to consume.

## Example Configuration
```agile_region: N
currency_unit: pence

devices:
  dishwasher:
    hours_required: 10
```

## MQTT Output Format
The above example configuration might lead to an output in this format:

### data/devices/dynamic_price/shelly:
```
  {
      "state": "on",
      "on_at:" <time>,
      "off_at": <time>
  }
```
### data/sensor/dynamic_price/upcoming_prices:
```
    {
        upcoming prices: <{times: price}>,
        current_price: <number>,
        next_price: <number>,
        previous_price: <number>
    }
  ```
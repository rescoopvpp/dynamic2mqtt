#!/bin/bash
if test "$COFYBOX_ENABLE_SERVICE" = TRUE ; then
    cp -n config/dynamic2mqtt_default_configuration.yaml /cofybox-config/dynamic2mqtt_configuration.yaml

    python dynamic2mqtt.py
else
    echo "Dynamic2MQTT Block not enabled by environment variable"
fi